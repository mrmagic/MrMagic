//
//  main.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/1.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
