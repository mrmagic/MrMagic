//
//  SelectCell.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/9.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "SelectCell.h"

@implementation SelectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
