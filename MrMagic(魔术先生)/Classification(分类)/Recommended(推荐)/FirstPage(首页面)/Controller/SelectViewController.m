//
//  SelectViewController.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/9.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "SelectViewController.h"
#import "SelectCell.h"

@interface SelectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *listView;
@end

@implementation SelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listView.estimatedRowHeight=44;
    self.listView.rowHeight = UITableViewAutomaticDimension;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier=@"Select";
    SelectCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"SelectCell" owner:nil options:nil] firstObject];
    }
    return cell;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
