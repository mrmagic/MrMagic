//
//  RecommendedListController.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/4.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "RecommendedListController.h"
#import "ZhiBoViewController.h"
#import "LiveListViewController.h"
#import <RongIMLib/RongIMLib.h>
#import "RCDLiveChatRoomViewController.h"
#import "VideoPlaybackController.h"

@interface RecommendedListController ()

@end

@implementation RecommendedListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)goToZhiBo:(UIButton *)sender {
    ZhiBoViewController *ZC=[[ZhiBoViewController alloc]init];
    [self presentViewController:ZC animated:YES completion:nil];
    
}

#pragma mark----直播列表
- (IBAction)GoToLiveList:(UIButton *)sender {
    LiveListViewController *LC=[[LiveListViewController alloc]init];
    [self presentViewController:LC animated:YES completion:nil];
    
}

#pragma makr----直播聊天室
- (IBAction)liveChatRoom:(UIButton *)sender {
//    NSString *token=@"/EvRhLkAArFS8upKj4wKAXe8N4sjd/ie4UH1KRvJbdBJNLzp9LNi8t/3ElgR4EUbgoPXaYQ2N6uRXE8yqYStrJqrZ24HlhiNUC+jVFi1gh0=";
    NSString *token=@"A+lCIAn0m17uz5JaseKHX2/EQx0JFL/jwfa+qs3Lwog+gB7VyjVwIA62qaFpdH/3lwPnWdTFZhRMhl9G/eFlR6fhGykaWQ4GI+1qOIz6yvQ=";
    [[RCIMClient sharedRCIMClient] connectWithToken:token success:^(NSString *loginUserId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"成功");
            RCUserInfo *user = [[RCUserInfo alloc]init];
            user.userId = @"1473067242.775831";
            user.portraitUri = @"zhenhuan.jpg";
            user.name = @"zhangjike";
            [RCIMClient sharedRCIMClient].currentUserInfo = user;
            RCDLiveChatRoomViewController *chatRoomVC = [[RCDLiveChatRoomViewController alloc]init];
            chatRoomVC.conversationType = ConversationType_CHATROOM;
            chatRoomVC.targetId = @"ChatRoom01";
            chatRoomVC.contentURL = @"rtmp://live.hkstv.hk.lxdns.com/live/hks";
            chatRoomVC.isScreenVertical = YES;
            [self.navigationController pushViewController:chatRoomVC animated:NO];
        });
    } error:^(RCConnectErrorCode status) {
        
            NSLog(@"失败");
        
    } tokenIncorrect:^{
        
            NSLog(@"过期");
        
    }];

}

- (IBAction)goToPlay:(UIButton *)sender {
    VideoPlaybackController *VP=[[VideoPlaybackController alloc]init];
    [self.navigationController pushViewController:VP animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
