//
//  ZhiBoViewController.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/1.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "ZhiBoViewController.h"
#import "AppDelegate.h"
#import <ZegoAVKit2/ZegoLiveApi.h>

@interface ZhiBoViewController () <ZegoLiveApiDelegate>

@property (nonatomic, weak) ZegoLiveApi *zegoAVApi;
@end

@implementation ZhiBoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.zegoAVApi = app.zegoAVApi;
    self.zegoAVApi.delegate = self;
    
    ZegoUser *user = [[ZegoUser alloc]init];
    user.userName=@"12222";
    user.userID=@"111";
    
    [self startPreview];
    
    [self.zegoAVApi loginChannel:@"12344444" user:user];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startPreview
{
    ZegoAVConfig *avConfig = [ZegoAVConfig defaultZegoAVConfig:ZegoAVConfigPreset_High];
    [self.zegoAVApi setAVConfig:avConfig];
    [self.zegoAVApi setFrontCam:YES];
    [self.zegoAVApi enableMic:YES];
    [self.zegoAVApi enableBeautifying:3];
    [self.zegoAVApi setLocalView:self.view];
    [self.zegoAVApi startPreview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/// \brief 登录频道的回调
- (void)onLoginChannel:(NSString *)channel error:(uint32)err {
    if (err==0) {
        // 登录成功，开始发布直播
        [self.zegoAVApi startPublishingWithTitle:@"魔术先生" streamID:@"12344444"];
    }else{
        // 登录频道失败，提示用户
        NSLog(@"%u",err);
    }
}

/// \brief 发布直播成功
- (void)onPublishSucc:(NSString *)streamID channel:(NSString *)channel streamInfo:(NSDictionary *)info {
    
    NSString *playUrl = [info[kZegoHlsUrlListKey] firstObject];
    
    NSLog(@"%s, stream: %@, url: %@", __func__, streamID, playUrl);
    
}

/// \brief 发布直播失败
/// \param err 1：正常结束，其它错误码可在集成指南最后参考
- (void)onPublishStop:(uint32)err stream:(NSString *)streamID channel:(NSString *)channel {
    
    NSLog(@"%s, stream: %@, err: %u", __func__, streamID, err);
    
}


#pragma mark-----关闭直播
- (IBAction)liveClose:(UIButton *)sender {
    // 结束直播
    [self.zegoAVApi stopPreview];
    [self.zegoAVApi stopPublishing];
    [self.zegoAVApi setLocalView:nil];
    [self.zegoAVApi logoutChannel];
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
