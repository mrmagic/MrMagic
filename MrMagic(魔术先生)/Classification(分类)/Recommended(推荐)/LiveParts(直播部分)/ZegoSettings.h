//
//  ZegoSettings.h
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/9/1.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZegoAVKit2/ZegoUser.h>

@interface ZegoSettings : NSObject
+ (instancetype)sharedInstance;
- (ZegoUser *)getZegoUser;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *userName;
@end
