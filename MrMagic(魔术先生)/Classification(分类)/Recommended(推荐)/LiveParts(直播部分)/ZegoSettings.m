//
//  ZegoSettings.m
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/9/1.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "ZegoSettings.h"

@implementation ZegoSettings
+ (instancetype)sharedInstance {
    static ZegoSettings *s_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [self new];
    });
    
    return s_instance;
}

- (ZegoUser *)getZegoUser
{
    ZegoUser *user = [ZegoUser new];
    user.userID = [ZegoSettings sharedInstance].userID;
    user.userName = [ZegoSettings sharedInstance].userName;
    
    return user;
}


@end
