//
//  UIImage+dealWithImage.h
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/5/16.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (dealWithImage)
/*图片压缩*/
-(NSData *)compactImage;
-(UIImage *)dealWithImage;

@end
