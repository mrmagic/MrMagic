//
//  UIColor+hexadecimalColor.h
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/4/7.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (hexadecimalColor)
+(UIColor *)colorWithHexString:(NSString *)hexString;

+(UIColor *)colorWithHexString:(NSString *)hexString andAlpha:(CGFloat)alpha;


@end
