//
//  UIView+layer.m
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/4/7.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "UIView+layer.h"

@implementation UIView (layer)
+(void)layerWithView:(UIView *)view WithColor:(UIColor *)color WithRadius:(CGFloat)Radius WithBorderWith:(CGFloat)BorderWidth{
    view.layer.cornerRadius=Radius;
    view.layer.borderWidth=BorderWidth;
    view.layer.borderColor=[color CGColor];
    view.layer.masksToBounds=YES;
}

-(void)layerWithColor:(UIColor *)color WithRadius:(CGFloat)Radius WithBorderWith:(CGFloat)BorderWidth{
    self.layer.cornerRadius=Radius;
    self.layer.borderWidth=BorderWidth;
    self.layer.borderColor=[color CGColor];
    self.layer.masksToBounds=YES;
}
@end
