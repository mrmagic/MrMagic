//
//  UIColor+hexadecimalColor.m
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/4/7.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "UIColor+hexadecimalColor.h"

@implementation UIColor (hexadecimalColor)
+(UIColor *)colorWithHexString:(NSString *)hexString{
    
    return [self colorWithHexString:hexString andAlpha:1];
}

+(UIColor *)colorWithHexString:(NSString *)hexString andAlpha:(CGFloat)alpha{
    
    if ([hexString length] != 7) {
        return [UIColor whiteColor];
    }else{
        
        NSRange range = NSMakeRange(1, 2);
        NSString * red = [hexString substringWithRange:range];
        range.location = 3;
        NSString * green = [hexString substringWithRange:range];
        range.location = 5;
        NSString * blue = [hexString substringWithRange:range];
        unsigned int r,g,b;
        [[NSScanner scannerWithString:red] scanHexInt:&r];
        [[NSScanner scannerWithString:green] scanHexInt:&g];
        [[NSScanner scannerWithString:blue] scanHexInt:&b];
        return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:alpha];
    }
}

@end
