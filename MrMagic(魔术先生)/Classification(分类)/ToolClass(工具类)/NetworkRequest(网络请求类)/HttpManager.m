//
//  HttpManager.m
//  公安局项目
//
//  Created by 徐恒 on 16/3/10.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#import "HttpManager.h"
#import "Usermanager.h"
#import "UIImage+dealWithImage.h"
static HttpManager * manager=nil;

@implementation HttpManager
+(HttpManager *)shareManager{
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            manager=[[self alloc]init];
        });
    return manager;
}

/**
 *  普通网络请求 传入info字典  URL短码 实现成功回调  失败回调
 *
 *  @param infoDic       info字典
 *  @param url           短码
 *  @param success_block 成功回调
 *  @param fail_block    失败回调
 */

-(NSURLSessionDataTask *)sendRequestWithDic:(NSMutableDictionary *)infoDic opt:(NSString *)opt shortURL:(NSString *)url setSuccessBlock:(void(^)(NSDictionary *responseDic))success_block setFailBlock:(void (^)(id obj))fail_block{
    NSURLSessionDataTask *operation=nil;
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:[Usermanager shareManager].token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"2" forHTTPHeaderField:@"platform"];
    [manager.requestSerializer setValue:@"1.0" forHTTPHeaderField:@"version"];
    NSString *usrStr=[NSString stringWithFormat:@"%@%@",BASE_URL,url];
    if ([opt isEqualToString:@"post"]) {
        [manager POST:usrStr parameters:infoDic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            HUDStop;
            success_block(responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            HUDNormal(@"连接服务器失败");
            NSLog(@"%@",error.description);
            fail_block(error.description);
            HUDStop;
            
        }];
    }
    if ([opt isEqualToString:@"get"]) {
        [manager GET:usrStr parameters:infoDic progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            HUDStop;
            success_block(responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            HUDNormal(@"连接服务器失败");
            NSLog(@"%@",error.description);
            fail_block(error.description);
            HUDStop;
        }];
        
    }
    return operation;
    
}

/**
 *  调用百度地图API
 *
 *  @param infoDic       info 字典参数
 *  @param opt           请求方式
 *  @param url           短的URL
 *  @param success_block 成功回调
 *  @param fail_block    失败回调
 *
 *  @return 返回operaion
 */
-(NSURLSessionDataTask *)sendRequestWithBaiduMapAPI:(NSMutableDictionary *)infoDic opt:(NSString *)opt shortURL:(NSString *)url setSuccessBlock:(void(^)(NSDictionary *responseDic))success_block setFailBlock:(void (^)(id obj))fail_block{
    NSURLSessionDataTask *operation=nil;
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *usrStr=[NSString stringWithFormat:@"%@%@",BaiduMap_URL,url];
    if ([opt isEqualToString:@"post"]) {
        [manager POST:usrStr parameters:infoDic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            HUDStop;
            success_block(responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            HUDNormal(@"连接服务器失败");
            NSLog(@"%@",error.description);
            fail_block(error.description);
            HUDStop;
            
        }];
    }
    
    if ([opt isEqualToString:@"get"]) {
        [manager GET:usrStr parameters:infoDic progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            HUDStop;
            success_block(responseObject);

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            HUDNormal(@"连接服务器失败");
            NSLog(@"%@",error.description);
            fail_block(error.description);
            HUDStop;
        }];
    }
    
    return operation;

}

/**
 *  上传图片  传入info字典  URL短码 实现成功回调  失败回调
 *
 *  @param infoDic       info字典
 *  @param urlStr        短码
 *  @param imageArray    图片数组
 *  @param success_block 成功回调
 *  @param fail_block    失败回调
 */
-(void)uploadImagesWithDic:(NSMutableDictionary *)infoDic shortURL:(NSString *)url images:(NSArray *)imageArray imageName:(NSString *)name setSuccessBlock:(void(^)(NSDictionary * responseDic))success_block setFailBlock:(void (^)(id obj))fail_block
{
    NSString *usrStr=[NSString stringWithFormat:@"%@%@",Image_URL,url];
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:usrStr parameters:infoDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            if (imageArray !=nil) {
    
                if (imageArray.count == 0) {
                    NSData *data  =[[NSData alloc]init];
                    [formData appendPartWithFileData:data name:name fileName:@"" mimeType:@"Multipart/form-data"];
                }
    
                for (int i =0; i<imageArray.count; i++) {
                    UIImage * image = imageArray[i];
                    NSLog(@"上次图片的时间%@",[self gettiem]);
                    NSData *data=[image compactImage];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"image" object:data];
                    [formData appendPartWithFileData:data name:[NSString stringWithFormat:@"%d",i] fileName:[NSString stringWithFormat:@"%@_%d.jpg",[self gettiem],i] mimeType:@"Multipart/form-data"];
                }
                
            }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           HUDStop;
            if (success_block) {
                success_block(responseObject);
            }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        HUDStop;
    }];
    
}

#pragma mark-------------获取当前时间字符串
-(NSString*)gettiem{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    NSString *dateTime=[formatter stringFromDate:[NSDate date]];
    return dateTime;
}


@end
