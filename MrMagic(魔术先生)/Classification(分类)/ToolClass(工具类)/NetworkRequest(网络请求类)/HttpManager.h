//
//  HttpManager.h
//  公安局项目
//
//  Created by 徐恒 on 16/3/10.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface HttpManager : NSObject
/**
 *  返回单例对象
 */
+(HttpManager *)shareManager;

/**
 *  普通网络请求 传人info字典参数 URL短码 实现成功回调 失败回调
 *
 *  @param infoDic       info 字典
 *  @param opt           请求方式(get/post/delete/put)
 *  @param url           短码
 *  @param success_block 成功回调
 *  @param fail_block    失败回调
 *
 *  @return 返回Operation
 */
-(NSURLSessionDataTask *)sendRequestWithDic:(NSMutableDictionary *)infoDic opt:(NSString *)opt shortURL:(NSString *)url setSuccessBlock:(void(^)(NSDictionary *responseDic))success_block setFailBlock:(void (^)(id obj))fail_block;

/**
 *  调用百度地图API
 *
 *  @param infoDic       info 字典参数
 *  @param opt           请求方式
 *  @param url           短的URL
 *  @param success_block 成功回调
 *  @param fail_block    失败回调
 *
 *  @return 返回operaion
 */
-(NSURLSessionDataTask *)sendRequestWithBaiduMapAPI:(NSMutableDictionary *)infoDic opt:(NSString *)opt shortURL:(NSString *)url setSuccessBlock:(void(^)(NSDictionary *responseDic))success_block setFailBlock:(void (^)(id obj))fail_block;

/**
 *  上传图片  传入info字典  URL短码 实现成功回调  失败回调
 *
 *  @param infoDic       info字典
 *  @param urlStr        短码
 *  @param imageArray    图片数组
 *  @param success_block 成功回调
 *  @param fail_block    失败回调
 */
-(void)uploadImagesWithDic:(NSMutableDictionary *)infoDic shortURL:(NSString *)url images:(NSArray *)imageArray imageName:(NSString *)name setSuccessBlock:(void(^)(NSDictionary * responseDic))success_block setFailBlock:(void (^)(id obj))fail_block;


@end
