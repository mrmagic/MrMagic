//
//  NoDataView.h
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/7/9.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoDataView : UIView
@property (weak, nonatomic) IBOutlet UILabel *Message;

@end
