//
//  CustonNavigaionController.m
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/4/9.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "CustonNavigaionController.h"

@interface CustonNavigaionController ()

@end

@implementation CustonNavigaionController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.barTintColor=[UIColor colorWithRed:0.976 green:0.988 blue:1.000 alpha:1.000];
    self.navigationBar.translucent=NO;
   //[self.tabBarItem setTitlePositionAdjustment:UIOffsetMake(20, -30)];
}
/**
 *  重写这个方法目的：能够拦截所有push进来的控制器
 *
 *  @param viewController 即将push进来的控制器
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count > 0) { // 此时push进来的viewController是第二个子控制器
        // 自动隐藏tabbar
        viewController.hidesBottomBarWhenPushed = YES;
    }
    // 调用父类pushViewController，self.viewControllers数组添加对象viewController
    [super pushViewController:viewController animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
