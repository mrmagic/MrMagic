//
//  RootViewController.m
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/4/9.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "RootViewController.h"
#import "CustonNavigaionController.h"
#import "PremiereViewController.h"
#import "FindListViewController.h"
#import "MylistViewController.h"
#import "PremiereViewController.h"
#import "SelectViewController.h"

@interface RootViewController ()<UITabBarDelegate>
//是否拨打客服
@property(nonatomic,assign)BOOL iscallServicer;
@property (nonatomic, strong)NSTimer *timer;
@property (nonatomic, assign)int seconds;
@property (nonatomic, strong)NSString *dateString;

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置文字的样式
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [UITabBar appearance].translucent = NO;
    NSDictionary *normalInfo=@{NSForegroundColorAttributeName:[UIColor colorWithWhite:0.400 alpha:1.000],NSFontAttributeName:[UIFont systemFontOfSize:9]};
     NSDictionary *selectedInfo=@{NSForegroundColorAttributeName:[UIColor colorWithWhite:0.600 alpha:1.000],NSFontAttributeName:[UIFont systemFontOfSize:9]};
    [[UITabBarItem appearance] setTitleTextAttributes:normalInfo forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:selectedInfo forState:UIControlStateSelected];
    [self addChildVc:[[SelectViewController alloc]init] title:@"精选" image:@"精选-未选中.png" selectedImage:@"精选-选中.png"];
    [self addChildVc:[[FindListViewController alloc]init] title:@"发现" image:@"发现-未选中.png" selectedImage:@"发现-选中.png"];
    [self addChildVc:[[MylistViewController alloc]init] title:@"我的" image:@"我的-未选中.png" selectedImage:@"我的-选中.png"];
    //[self addChildVc:[[PremiereViewController alloc]init] title:@"开播" image:@"开播-未选中.png" selectedImage:@"开播-选中.png"];
}

/**
 *  添加一个子控制器
 *
 *  @param childVc       子控制器
 *  @param title         标题
 *  @param image         图片
 *  @param selectedImage 选中的图片
 */
- (void)addChildVc:(UIViewController *)childVc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    // 设置子控制器的文字
    childVc.title = title; // 同时设置tabbar和navigationBar的文字
    // 设置子控制器的图片
    childVc.tabBarItem.image = [UIImage imageNamed:image];
    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.tag=1000+self.viewControllers.count;
       // 先给外面传进来的小控制器 包装 一个导航控制器
    CustonNavigaionController *nav = [[CustonNavigaionController alloc] initWithRootViewController:childVc];
     //调整文字的距离
     [childVc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -3)];
    // 添加为子控制器
    [self addChildViewController:nav];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
