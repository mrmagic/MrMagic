//
//  CutomButton.h
//  heheh
//
//  Created by 镇景雄 on 16/4/12.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CutomButton : UIButton
@property(nonatomic,strong)NSDictionary *info;
@property (nonatomic, strong)NSIndexPath *indexPath;
@property(nonatomic,assign)NSInteger section;
@property (nonatomic, assign)BOOL isSelected;
@end
