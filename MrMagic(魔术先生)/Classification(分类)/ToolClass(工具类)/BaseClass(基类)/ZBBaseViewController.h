//
//  ZBBaseViewController.h
//  公安局项目
//
//  Created by 徐恒 on 16/3/11.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZBBaseViewController : UIViewController

@property(nonatomic,strong)NSString *titleStr;

-(void)setLeftButton:(id)LeftButton;

-(void)setRightButton:(id)rightButton;

-(void)setLeftView;
-(void)back;

-(void)goBack;

- (BOOL)stringIsEmpty:(NSString *)string;

- (void)changeScoreValueWithscore:(NSString *)score scoreType:(NSString *)scoreType scoreValue:(NSString *)scoreValue;

-(void)rightButtonClick;
-(void)leftButtonClick;

- (void)checkBigPhoto:(NSArray *)photoList indexPath:(NSIndexPath *)indexPath;

#pragma mark----设置导航栏的标题试图
-(void)setTitleView:(UIView *)ItemView;


- (UIImage*) createImageWithColor: (UIColor*) color;




@end
