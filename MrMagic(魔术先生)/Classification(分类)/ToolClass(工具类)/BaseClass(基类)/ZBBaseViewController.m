//
//  ZBBaseViewController.m
//  公安局项目
//
//  Created by 徐恒 on 16/3/11.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#import "ZBBaseViewController.h"
#import "UIBarButtonItem+Extension.h"
#import "NSDate+currentDate.h"
#import "UIButton+WebCache.h"
#import "IQKeyboardManager.h"


@interface ZBBaseViewController ()

@end

@implementation ZBBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //键盘注销设置
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    self.edgesForExtendedLayout=UIRectEdgeNone;
    if (self.navigationController.viewControllers.count>1) {
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"二级导航-返回.png"] imageWithRenderingMode:/*去除渲染效果*/UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    }
    //设置标题样式
    NSDictionary *titleDict=@{NSFontAttributeName:[UIFont boldSystemFontOfSize:20],NSForegroundColorAttributeName:[UIColor colorWithWhite:0.200 alpha:1.000]};
    [self.navigationController.navigationBar setTitleTextAttributes:titleDict];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
}

#pragma mark----调用此方法，返回到上一个页面
-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setLeftView{
    UIButton *CustomView=[[UIButton alloc]initWithFrame:CGRectMake(0, 10, 100, 30)];
    //CustomView.backgroundColor=[UIColor redColor];
    UIImageView *leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 20)];
    //leftImageView.backgroundColor=[UIColor greenColor];
    leftImageView.image=[UIImage imageNamed:@"返回.png"];
    UILabel *textLable=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, 50, 30)];
    //textLable.backgroundColor=[UIColor blueColor];
    textLable.textAlignment=NSTextAlignmentCenter;
    textLable.font=[UIFont systemFontOfSize:16.5];
    textLable.textColor=[UIColor whiteColor];
    textLable.text=@"返回";
    [CustomView addSubview:leftImageView];
    [CustomView addSubview:textLable];
    [CustomView addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:CustomView];
    
}

#pragma mark------返回
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-----设置左边按钮
-(void)setLeftButton:(id)LeftButton{
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = [UIColor colorWithWhite:0.992 alpha:1.000];
    textAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:16.5];
    if ([LeftButton isKindOfClass:[UIImage class]]) {
        self.navigationItem.leftBarButtonItem=[UIBarButtonItem itemWithTarget:self action:@selector(leftButtonClick) image:LeftButton highImage:LeftButton];
        [self.navigationItem.leftBarButtonItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
        return;
    }
    if ([LeftButton isKindOfClass:[NSString class]]) {
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:LeftButton style:UIBarButtonItemStylePlain target:self action:@selector(leftButtonClick)];
        [self.navigationItem.leftBarButtonItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
        return;
    }
}

#pragma mark-----点击左按钮出发事情
-(void)leftButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-----设置右边按钮
-(void)setRightButton:(id)rightButton{
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = [UIColor colorWithWhite:0.992 alpha:1.000];
    textAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:16.5];
    if ([rightButton isKindOfClass:[UIImage class]]) {
        self.navigationItem.rightBarButtonItem=[UIBarButtonItem itemWithTarget:self action:@selector(rightButtonClick) image:rightButton highImage:rightButton];
        [self.navigationItem.rightBarButtonItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    }
    if ([rightButton isKindOfClass:[NSString class]]) {
        self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:rightButton style:UIBarButtonItemStylePlain target:self action:@selector(rightButtonClick)];
        [self.navigationItem.rightBarButtonItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    }
}

#pragma mark----设置导航栏的标题试图
-(void)setTitleView:(UIView *)ItemView{
    self.navigationItem.titleView=ItemView;
}


#pragma mark-----点击右按钮出发事情
-(void)rightButtonClick{
    
    
}

#pragma mark 判断字符串是否为空或者为空格
- (BOOL)stringIsEmpty:(NSString *)string {
    if (!string) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [string stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}


- (UIImage*) createImageWithColor: (UIColor*) color
{
    CGRect rect=CGRectMake(0,0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
