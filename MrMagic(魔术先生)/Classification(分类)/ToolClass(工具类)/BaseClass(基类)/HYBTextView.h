//
//  HYBTextView.h
//  HealthyWalk
//
//  Created by liyanpeng on 15/1/31.
//  Copyright (c) 2015年 LC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HYBTextView : UITextView
/*!
 * @brief 占位符文本,与UITextField的placeholder功能一致
 */
@property (nonatomic, strong) NSString *placeholder;

/*!
 * @brief 占位符文本颜色
 */
@property (nonatomic, strong) UIColor *placeholderTextColor;

@end
