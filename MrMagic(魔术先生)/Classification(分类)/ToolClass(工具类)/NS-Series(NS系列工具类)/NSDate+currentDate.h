//
//  NSDate+currentDate.h
//  Dixon
//
//  Created by lcsj on 15/4/11.
//  Copyright (c) 2015年 LONGPF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (currentDate)
+(NSString *)getCurrentTime;
+(NSDate *)dateFromString:(NSString *)dateString;
-(NSString *)getTimeString;
+(NSString *)getWeek:(NSString *)TimeString;
-(NSString *)getTimeStringOfday;

@end
