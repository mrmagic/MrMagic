//
//  NSString+HTML.h
//  HongHuaMedical
//
//  Created by Ross Xiao on 15/11/5.
//  Copyright (c) 2015年 Ross Xiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTML)
- (NSString *)removeHTML;
@end
