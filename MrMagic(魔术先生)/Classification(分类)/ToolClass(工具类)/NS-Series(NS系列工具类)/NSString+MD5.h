//
//  NSString+MD5.h
//  BlueSinoDev
//
//  Created by wangpengfei on 13-3-12.
//  Copyright (c) 2013年 wangpengfei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface NSString (MD5)

- (NSString *)md5;

@end
