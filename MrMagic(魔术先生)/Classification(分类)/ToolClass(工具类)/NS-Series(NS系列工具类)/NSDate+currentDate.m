//
//  NSDate+currentDate.m
//  Dixon
//
//  Created by lcsj on 15/4/11.
//  Copyright (c) 2015年 LONGPF. All rights reserved.
//

#import "NSDate+currentDate.h"

@implementation NSDate (currentDate)
+(NSString *)getCurrentTime{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime=[formatter stringFromDate:[NSDate date]];
    return dateTime;
}

+(NSDate *)dateFromString:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

-(NSString *)getTimeString{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime=[formatter stringFromDate:self];
    return dateTime;
}

-(NSString *)getTimeStringOfday{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTime=[formatter stringFromDate:self];
    return dateTime;
}


+(NSString *)getWeek:(NSString *)TimeString{
    NSString *year = [TimeString substringToIndex:4];
    NSString *str = [TimeString substringFromIndex:5];
    NSString *month = [str substringToIndex:2];
    NSString *str1 = [str substringFromIndex:3];
    NSString *day  =[str1 substringToIndex:2];
//    NSString *str2 = [str1 substringFromIndex:2];
//    NSString *time = [str2 substringToIndex:6];
    
    NSDateComponents *_comps = [[NSDateComponents alloc] init];
    [_comps setDay:[day integerValue]];
    [_comps setMonth:[month integerValue]];
    [_comps setYear:[year integerValue]];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *_date = [gregorian dateFromComponents:_comps];
    NSString *week = [self weekdayStringFromDate:_date];
    return week;
}

+(NSString *)weekdayStringFromDate:(NSDate*)inputDate {
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"星期日", @"星期一", @"星期二", @"星期三", @"星期四", @"星期五", @"星期六", nil];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSWeekdayCalendarUnit;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    
    return [weekdays objectAtIndex:theComponents.weekday];
    
}

@end
