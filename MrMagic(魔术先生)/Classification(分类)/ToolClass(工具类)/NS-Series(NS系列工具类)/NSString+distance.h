//
//  NSString+distance.h
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/7/7.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (distance)
-(NSString *)getDistanceStr;
@end
