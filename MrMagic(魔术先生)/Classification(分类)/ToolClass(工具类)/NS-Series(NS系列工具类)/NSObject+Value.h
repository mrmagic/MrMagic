//
//  NSObject+Value.h
//  QSQ
//
//  Created by xiafan on 13-8-24.
//  Copyright (c) 2013年 xiafan. All rights reserved.
//


#define QSQSetter(className, subfix, param) \
- (void)set##subfix:(id)param \
{ \
    if ([param isKindOfClass:[NSDictionary class]]) { \
        _##param = [[className alloc] initWithDict:param]; \
    } else { \
        _##param = param; \
    } \
}


#define QSQWithDictH(prefix) \
+ (instancetype)prefix##WithDict:(NSDictionary *)dict; \
- (instancetype)initWithDict:(NSDictionary *)dict;

#define QSQWithDictM(prefix) \
- (instancetype)initWithDict:(NSDictionary *)dict \
{ \
    if (self = [super init]) { \
        [self setValues:dict]; \
    } \
    return self; \
} \
+ (instancetype)prefix##WithDict:(NSDictionary *)dict \
{ \
    return [[self alloc] initWithDict:dict]; \
}


#define QSQEncodeDecodeModel \
- (id)initWithCoder:(NSCoder *)decoder \
{ \
    if (self = [super init]) { \
        [self decode:decoder]; \
    } \
    return self; \
} \
\
- (void)encodeWithCoder:(NSCoder *)encoder \
{ \
    [self encode:encoder]; \
}


#import <Foundation/Foundation.h>

@interface NSObject (Value)
//
- (void)setValues:(NSDictionary *)values;
- (NSDictionary *)values;
- (void)decode:(NSCoder *)decoder;
- (void)encode:(NSCoder *)encoder;
@end

