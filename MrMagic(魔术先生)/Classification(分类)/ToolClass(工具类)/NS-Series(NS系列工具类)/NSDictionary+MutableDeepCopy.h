//
//  NSDictionary+MutableDeepCopy.h
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/6/16.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MutableDeepCopy)
-(NSMutableDictionary *)mutableDeepCopy;
//增加mutableDeepCopy方法
@end
