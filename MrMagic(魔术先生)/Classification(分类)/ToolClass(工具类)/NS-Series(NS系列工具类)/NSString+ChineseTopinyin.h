//
//  NSString+ChineseTopinyin.h
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/6/17.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ChineseTopinyin)
-(BOOL)containsThePinyin:(NSString *)pinStr firstLetter:(NSString *)firstLetter;
@end
