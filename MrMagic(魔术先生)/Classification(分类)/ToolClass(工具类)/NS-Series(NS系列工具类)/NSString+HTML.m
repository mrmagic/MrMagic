//
//  NSString+HTML.m
//  HongHuaMedical
//
//  Created by Ross Xiao on 15/11/5.
//  Copyright (c) 2015年 Ross Xiao. All rights reserved.
//

#import "NSString+HTML.h"

@implementation NSString (HTML)
- (NSString *)removeHTML{
    NSScanner *theScanner;
    NSString *text = nil;
    NSString *str=self;
    theScanner = [NSScanner scannerWithString:str];
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@" "];
    }
    return str;
}

@end
