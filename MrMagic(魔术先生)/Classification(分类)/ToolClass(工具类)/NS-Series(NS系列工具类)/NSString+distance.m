//
//  NSString+distance.m
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/7/7.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "NSString+distance.h"

@implementation NSString (distance)
-(NSString *)getDistanceStr{
    NSString *str;
    if (self.integerValue>=1000) {
        str=[NSString stringWithFormat:@"%0.1f千米",self.integerValue/1000.0f];
    }else{
        str=[NSString stringWithFormat:@"%@千米",self];
    }
    return str;
}
@end
