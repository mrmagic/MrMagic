//
//  NSString+ChineseTopinyin.m
//  智能社区(用户)
//
//  Created by 镇景雄 on 16/6/17.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "NSString+ChineseTopinyin.h"
#import "pinyin.h"

@implementation NSString (ChineseTopinyin)
-(BOOL)containsThePinyin:(NSString *)pinStr firstLetter:(NSString *)firstLetter{
    //让通讯录的名字成可变字符串
    NSMutableString *str=[NSMutableString stringWithString:self];
    //转换成拼音
    CFStringTransform((__bridge CFMutableStringRef)str, NULL, kCFStringTransformMandarinLatin, NO);
    //去掉音标
        CFStringTransform((__bridge CFMutableStringRef)str, NULL, kCFStringTransformStripDiacritics, NO);
    NSLog(@"pinyin= %@",self);
    //将输入的字符串转化为小写
    NSString *inputStr=pinStr.lowercaseString;
    NSRange range=[str rangeOfString:inputStr];
    if (![inputStr.uppercasePinYinFirstLetter isEqualToString:firstLetter]) {
        return NO;
    }else{
        if(range.location !=NSNotFound)//_roaldSearchText
        {
            if (range.location==0) {
                return YES;
            }else{
                return NO;
            }
            
        }else
        {
            return NO;
        }
    }
    
}
@end
