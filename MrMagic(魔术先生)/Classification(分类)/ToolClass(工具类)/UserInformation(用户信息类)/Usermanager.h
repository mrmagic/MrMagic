//
//  Usermanager.h
//  公安局项目
//
//  Created by 镇景雄 on 16/3/22.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Usermanager : NSObject

/**
 *  返回单例对象
 */
+(Usermanager *)shareManager;
/**
 *  用户登录获取的个人信息数据
 */
@property(nonatomic,strong)NSMutableDictionary *userInfo;
//维度
@property(nonatomic,strong)NSString *latitude;
//经度
@property(nonatomic,strong)NSString *longitude;
//token
@property(nonatomic,strong)NSString *token;


@end
