//
//  Usermanager.m
//  公安局项目
//
//  Created by 镇景雄 on 16/3/22.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#import "Usermanager.h"
static Usermanager * manager=nil;

@implementation Usermanager
+(Usermanager *)shareManager{
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        manager=[[self alloc]init];
    });
    return manager;
}


@end
