//
//  PremiereViewController.m
//  魔术先生
//
//  Created by zhangyi on 16/8/31.
//  Copyright © 2016年 开眼文化娱乐有限公司. All rights reserved.
//

#import "PremiereViewController.h"
#import "ZhiBoViewController.h"
#import "LiveListViewController.h"
#import "LoginViewController.h"

@interface PremiereViewController ()

@end

@implementation PremiereViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)goToZhiBo:(UIButton *)sender {
    ZhiBoViewController *ZC=[[ZhiBoViewController alloc]init];
    [self presentViewController:ZC animated:YES completion:nil];
    
}

#pragma mark----直播列表
- (IBAction)GoToLiveList:(UIButton *)sender {
    LiveListViewController *LC=[[LiveListViewController alloc]init];
    [self presentViewController:LC animated:YES completion:nil];
    
}

- (IBAction)login:(UIButton *)sender {
    LoginViewController *LC=[[LoginViewController alloc]init];
    [self.navigationController pushViewController:LC animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
