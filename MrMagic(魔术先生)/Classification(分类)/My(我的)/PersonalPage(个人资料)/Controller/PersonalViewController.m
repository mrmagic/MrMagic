//
//  PersonalViewController.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/7.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "PersonalViewController.h"

@interface PersonalViewController ()

@end

@implementation PersonalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"个人资料";
}

- (IBAction)updateUserInformation:(UIButton *)sender {
    NSMutableDictionary *info=[NSMutableDictionary new];
    [info setValue:@"13308622037" forKey:@"userName"];
    [info setValue:@"test" forKey:@"name"];
    [info setValue:@"/uploadFiles/uploadImgs/test.jpg" forKey:@"avatar"];
    [info setValue:@"1" forKey:@"gender"];
    [info setValue:@"个性签名" forKey:@"signature"];
    [[HttpManager shareManager] sendRequestWithDic:info opt:@"post" shortURL:@"/api/appUser/update" setSuccessBlock:^(NSDictionary *responseDic) {
        NSLog(@"%@",responseDic);
        HUDNormal(responseDic[@"msg"]);
        if ([responseDic[@"code"] integerValue]==0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } setFailBlock:^(id obj) {
        
    }];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
