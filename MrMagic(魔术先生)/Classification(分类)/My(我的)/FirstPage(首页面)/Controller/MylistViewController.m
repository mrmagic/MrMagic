//
//  MylistViewController.m
//  魔术先生
//
//  Created by zhangyi on 16/8/31.
//  Copyright © 2016年 开眼文化娱乐有限公司. All rights reserved.
//

#import "MylistViewController.h"
#import "LoginViewController.h"
#import "PersonalViewController.h"
#import "MagicianCertificationController.h"
#import "MyListCell.h"
#import "UIView+layer.h"

@interface MylistViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UITableView *listView;
@property(nonatomic,strong)NSArray *iTemArray;
@property(nonatomic,strong)NSArray *imageArray;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation MylistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listView.estimatedRowHeight=10;
    self.listView.rowHeight = UITableViewAutomaticDimension;
    self.listView.tableHeaderView=self.headView;
    self.iTemArray=@[@"",@"消息",@"",@"我要认证魔术师",@"",@"帮助反馈",@"设置"];
    self.imageArray=@[@"",@"消息.png",@"",@"认证魔术师.png",@"",@"帮助反馈.png",@"设置.png"];
    [self.loginButton layerWithColor:[UIColor clearColor] WithRadius:3 WithBorderWith:0.5];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0 || indexPath.row==2 || indexPath.row==4) {
        return 10;
    }else{
        return 50;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0 || indexPath.row==2 || indexPath.row==4) {
        static NSString *identifier=@"MyList2";
        MyListCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell=[[NSBundle mainBundle] loadNibNamed:@"MyListCell" owner:nil options:nil][1];
        }
        return cell;
        
    }else{
        static NSString *identifier=@"MyList1";
        MyListCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell=[[NSBundle mainBundle] loadNibNamed:@"MyListCell" owner:nil options:nil][0];
        }
        cell.pictureImage.image=[UIImage imageNamed:self.imageArray[indexPath.row]];
        cell.iTem.text=self.iTemArray[indexPath.row];
        if (indexPath.row==5) {
            cell.lineImage.hidden=NO;
        }else{
            cell.lineImage.hidden=YES;
        }
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 1:
        {
            HUDNormal(@"消息");
        }
            break;
        case 3:{
            MagicianCertificationController *MC=[[MagicianCertificationController alloc]init];
            [self.navigationController pushViewController:MC animated:YES];
        }
            break;
        case 5:{
            HUDNormal(@"帮助反馈");
        }
            break;
        case 6:{
            HUDNormal(@"帮助反馈");
        }
            break;
            
        default:
            break;
    }

}

#pragma mark----登录
- (IBAction)login:(UIButton *)sender {
    LoginViewController *LC=[[LoginViewController alloc]init];
    [self.navigationController pushViewController:LC animated:YES];
}

#pragma mark---更新内容
- (IBAction)updateTheContent:(UIButton *)sender {
    PersonalViewController *PVC=[[PersonalViewController alloc]init];
    [self.navigationController pushViewController:PVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
