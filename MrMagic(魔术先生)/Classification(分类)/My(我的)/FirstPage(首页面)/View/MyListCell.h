//
//  MyListCell.h
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/8.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *iTem;
@property (weak, nonatomic) IBOutlet UIImageView *pictureImage;
@property (weak, nonatomic) IBOutlet UIImageView *lineImage;


@end
