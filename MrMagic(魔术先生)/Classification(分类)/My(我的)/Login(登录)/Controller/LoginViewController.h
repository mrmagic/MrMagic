//
//  LoginViewController.h
//  智能社区(用户)
//
//  Created by 董杰 on 16/4/13.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "ZBBaseViewController.h"
#import "CutomButton.h"

@interface LoginViewController : ZBBaseViewController
- (IBAction)loginAction:(CutomButton *)sender;
@end
