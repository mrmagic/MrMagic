//
//  LoginViewController.m
//  智能社区(用户)
//
//  Created by 董杰 on 16/4/13.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "ForgetViewController.h"
#import "RootViewController.h"
#import "Usermanager.h"
#import "CutomButton.h"
#import "NSString+Addition.h"

@interface LoginViewController ()<UITextFieldDelegate>
@property (nonatomic, strong)RegisterViewController *registerVC;
@property (nonatomic, strong)ForgetViewController *forgetVC;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *passWord;


@end

@implementation LoginViewController

//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden=YES;
//}
//-(void)viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.hidden=NO;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"登录";
    NSDictionary *info=[UserDefault objectForKey:@"userInfo"];
    if (info) {
        self.phoneNumber.text=info[@"phoneNumber"];
        self.passWord.text=info[@"passWord"];
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.text.length==0) {
        self.passWord.text=@"";
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneNumber) {
        if (string.length == 0) return YES;
        
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if (existedLength - selectedLength + replaceLength > 11) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark 注销键盘
- (void)resignKboard {
    [self.phoneNumber resignFirstResponder];
    [self.passWord resignFirstResponder];
}

#pragma mark 忘记密码按钮点击事件
- (IBAction)forgetPassWordAction:(UIButton *)sender {
    [self resignKboard];
    //sender.isSelected=YES;
    self.forgetVC = [[ForgetViewController alloc] init];
    [self.navigationController pushViewController:self.forgetVC animated:YES];
}


#pragma mark 登录按钮点击事件
- (IBAction)loginAction:(CutomButton *)sender {
    [self resignKboard];
    if ([self.phoneNumber.text isValidateMobile]) {
        HUDNoStop1(@"正在登录中.....");
        NSMutableDictionary *info=[[NSMutableDictionary alloc]init];
        [info setValue:self.phoneNumber.text forKey:@"userName"];
        [info setValue:self.passWord.text forKey:@"password"];
        [[HttpManager shareManager] sendRequestWithDic:info opt:@"post" shortURL:@"/api/appUser/login" setSuccessBlock:^(NSDictionary *responseDic) {
            NSLog(@"%@",responseDic);
            if (requestIsSuccessful) {
                [Usermanager shareManager].token=responseDic[@"token"];
                [self.navigationController popViewControllerAnimated:YES];
                } else {
                    
                                    
            }
            
        } setFailBlock:^(id obj) {
            
        }];
    } else {
        HUDNormal(@"请输入正确的手机号")
    }
}

#pragma mark 注册按钮点击事件
- (IBAction)registerAction:(UIButton *)sender {
    [self resignKboard];
    self.registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:self.registerVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
