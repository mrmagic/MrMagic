//
//  RegisterViewController.m
//  智能社区(用户)
//
//  Created by 董杰 on 16/4/13.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "RegisterViewController.h"
#import "NSString+Addition.h"
#import "IQKeyboardManager.h"

@interface RegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *VerificationCode;
@property (weak, nonatomic) IBOutlet UITextField *passWord;
@property (weak, nonatomic) IBOutlet UIButton *verificationButton;
@property (nonatomic, assign)int second;
@property (nonatomic, strong)NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIView *securityBackground;
@property (nonatomic, strong)UILabel *timeLabel;
@property (nonatomic, assign)BOOL isStartTime;
@property (weak, nonatomic) IBOutlet UIImageView *successImage;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"注册";
    [self.phoneNumber addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //键盘注销设置
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
   
}

-(void)textFieldDidChange :(UITextField *)textField{
    if ([textField.text isValidateMobile]) {
        self.successImage.hidden = NO;
    } else {
        self.successImage.hidden = YES;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneNumber) {
        if (string.length == 0) return YES;
        
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if (existedLength - selectedLength + replaceLength > 11) {
            return NO;
        }
    }
    
    return YES;
}

- (void)setLaebl {
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.verificationButton.frame.origin.x, self.verificationButton.frame.origin.y, self.verificationButton.size.width, self.verificationButton.size.height)];
    self.timeLabel.backgroundColor = [UIColor orangeColor];
    self.timeLabel.text = @"重新发送60s";
    self.timeLabel.layer.masksToBounds = YES;
    self.timeLabel.layer.cornerRadius = 10;
    self.timeLabel.textColor = [UIColor whiteColor];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    [self.securityBackground addSubview:self.timeLabel];
    [self.securityBackground bringSubviewToFront:self.timeLabel];
}

#pragma mark 注销键盘
- (void)resignTextField {
    [self.phoneNumber resignFirstResponder];
    [self.VerificationCode resignFirstResponder];
    [self.passWord resignFirstResponder];
}

#pragma mark 获取验证码按钮点击事件
- (IBAction)getVerificationCode:(UIButton *)sender {
    [self resignTextField];
    if ([self.phoneNumber.text isValidateMobile]) {
        NSMutableDictionary *info=[NSMutableDictionary new];
        [info setValue:self.phoneNumber.text forKey:@"mobile"];
        [info setValue:@"1" forKey:@"type"];
        [self setLaebl];
        //设置倒计时时长
        self.second = 60;
        self.verificationButton.enabled = NO;
        //设置倒计时
        if (!self.isStartTime) {
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
        }
        [[HttpManager shareManager] sendRequestWithDic:info opt:@"post" shortURL:@"/api/appUser/sendCode" setSuccessBlock:^(NSDictionary *responseDic) {
            NSLog(@"%@",responseDic);
            if ([responseDic[@"code"] integerValue]==1) {
                NSString *str=[NSString stringWithFormat:@"你的验证码%@",responseDic[@"result"]];
                HUDNormal2(str);
            }
            
        } setFailBlock:^(id obj) {
            
        }];
    } else {
        HUDNormal(@"请输入正确的手机号")
    }
}

- (void)timerAction {
    self.isStartTime = YES;
    self.second--;
    self.timeLabel.text =[NSString stringWithFormat:@"重新发送%ds", self.second];
    if (self.second == -1) {
        [self.verificationButton setTitle:@"重新获取验证码" forState:UIControlStateNormal];
        self.verificationButton.enabled = YES;
        [self.timer setFireDate:[NSDate distantFuture]];
        self.isStartTime = NO;
        [self.timeLabel removeFromSuperview];
    }
}

#pragma mark 提交注册
- (IBAction)Register:(UIButton *)sender {
    [self resignTextField];
    if ([self.phoneNumber.text isValidateMobile]) {
        HUDNoStop1(@"正在注册...")
        NSMutableDictionary *info=[NSMutableDictionary new];
        [info setValue:self.phoneNumber.text forKey:@"userName"];
        [info setValue:self.passWord.text forKey:@"password"];
        [info setValue:self.VerificationCode.text forKey:@"code"];
        [[HttpManager shareManager] sendRequestWithDic:info opt:@"post" shortURL:@"/api/appUser/register" setSuccessBlock:^(NSDictionary *responseDic) {
            NSLog(@"%@",responseDic);
            if ([responseDic[@"code"] integerValue]==1) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } setFailBlock:^(id obj) {
            
        }];
    } else {
        HUDNormal(@"请输入正确的手机号")
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
