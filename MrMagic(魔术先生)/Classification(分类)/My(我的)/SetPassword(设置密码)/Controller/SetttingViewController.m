//
//  SetttingViewController.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/8.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "SetttingViewController.h"

@interface SetttingViewController ()
@property (weak, nonatomic) IBOutlet UITextField *passWord;


@end

@implementation SetttingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"设置密码";
    
}

- (IBAction)submit:(UIButton *)sender {
    NSMutableDictionary *info=[NSMutableDictionary new];
    [info setValue:@"13308622037" forKey:@"userName"];
    [info setValue:self.passWord.text forKey:@"password"];
    [[HttpManager shareManager] sendRequestWithDic:info opt:@"post" shortURL:@"/api/appUser/update" setSuccessBlock:^(NSDictionary *responseDic) {
        NSLog(@"%@",responseDic);
        HUDNormal(responseDic[@"msg"]);
        if ([responseDic[@"code"] integerValue]==1) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    } setFailBlock:^(id obj) {
        
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
