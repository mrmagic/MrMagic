//
//  ForgetViewController.m
//  智能社区(用户)
//
//  Created by 董杰 on 16/4/13.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "ForgetViewController.h"
#import "NSString+Addition.h"
#import "SetttingViewController.h"

@interface ForgetViewController ()
@property (weak, nonatomic) IBOutlet UITextField *telephone;
@property (weak, nonatomic) IBOutlet UIView *securityBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *getVerificationButton;
@property (nonatomic, assign)int second;
@property (nonatomic, strong)NSTimer *timer;
@property (nonatomic, strong)UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UITextField *verificationCode;
@property (weak, nonatomic) IBOutlet UIImageView *successImage;
@property (nonatomic, assign)BOOL isStartTime;

@end

@implementation ForgetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"密码找回";
    [self.telephone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}

-(void)textFieldDidChange :(UITextField *)textField{
    if ([textField.text isValidateMobile]) {
        self.successImage.hidden = NO;
    } else {
        self.successImage.hidden = YES;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.telephone) {
        if (string.length == 0) return YES;
        
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if (existedLength - selectedLength + replaceLength > 11) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark 注销键盘
- (void)resignTextField {
    [self.telephone resignFirstResponder];
    [self.verificationCode resignFirstResponder];
}

#pragma mark 获取验证码按钮点击事件
- (IBAction)getVerificationCode:(UIButton *)sender {
    [self resignTextField];
    if ([self.telephone.text isValidateMobile]) {
        NSMutableDictionary *info=[NSMutableDictionary new];
        [info setValue:self.telephone.text forKey:@"mobile"];
        [info setValue:@"3" forKey:@"type"];
        [self setLaebl];
        //设置倒计时时长
        self.second = 60;
        self.getVerificationButton.enabled = NO;
        //设置倒计时
        if (!self.isStartTime) {
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
        }
        [[HttpManager shareManager] sendRequestWithDic:info opt:@"post" shortURL:@"/api/appUser/sendCode" setSuccessBlock:^(NSDictionary *responseDic) {
            NSLog(@"%@",responseDic);
            HUDNormal(responseDic[@"result"]);
            if ([responseDic[@"code"] integerValue]==1) {
                NSString *str=[NSString stringWithFormat:@"你的验证码%@",responseDic[@"result"]];
                HUDNormal2(str);
            }
            
        } setFailBlock:^(id obj) {
            
        }];
    } else {
        HUDNormal(@"请输入正确的手机号")
    }
}

#pragma mark 提交按钮点击事件
- (IBAction)presentAction:(UIButton *)sender {
    [self resignTextField];
    if ([self.telephone.text isValidateMobile]) {
        NSMutableDictionary *info=[NSMutableDictionary new];
        [info setValue:self.telephone.text forKey:@"userName"];
        [info setValue:self.verificationCode.text forKey:@"code"];
        [[HttpManager shareManager] sendRequestWithDic:info opt:@"post" shortURL:@"/api/appUser/findPassword" setSuccessBlock:^(NSDictionary *responseDic) {
            NSLog(@"%@",responseDic);
            HUDNormal(responseDic[@"msg"]);
            if ([responseDic[@"code"] integerValue]==1) {
                SetttingViewController *SC=[[SetttingViewController alloc]init];
                [self.navigationController pushViewController:SC animated:YES];
                
            }
            
        } setFailBlock:^(id obj) {
            
        }];
    } else {
        HUDNormal(@"请输入正确的手机号")
    }
}

- (void)setLaebl {
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.getVerificationButton.frame.origin.x, self.getVerificationButton.frame.origin.y, self.getVerificationButton.size.width, self.getVerificationButton.size.height)];
    self.timeLabel.backgroundColor = [UIColor orangeColor];
    self.timeLabel.text = @"重新发送60s";
    self.timeLabel.layer.masksToBounds = YES;
    self.timeLabel.layer.cornerRadius = 5;
    self.timeLabel.textColor = [UIColor whiteColor];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    [self.securityBackgroundView addSubview:self.timeLabel];
}

- (void)timerAction {
    self.second--;
    self.timeLabel.text =[NSString stringWithFormat:@"重新发送%ds", self.second];
    if (self.second == -1) {
        [self.getVerificationButton setTitle:@"重新获取验证码" forState:UIControlStateNormal];
        self.getVerificationButton.enabled = YES;
        [self.timer setFireDate:[NSDate distantFuture]];
        [self.timeLabel removeFromSuperview];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
