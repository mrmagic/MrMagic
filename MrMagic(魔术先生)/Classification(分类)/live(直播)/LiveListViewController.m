//
//  LiveListViewController.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/2.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "LiveListViewController.h"
#import "AppDelegate.h"
#import <ZegoAVKit2/ZegoLiveApi.h>

@interface LiveListViewController ()<ZegoLiveApiDelegate>
@property (nonatomic, weak) ZegoLiveApi *zegoAVApi;

@end

@implementation LiveListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.zegoAVApi = app.zegoAVApi;
    // 设置回调
    self.zegoAVApi.delegate = self;
    // 登录频道，sdk会在onLoginChannel回调中返回登陆结果
    // loginChannel这个步骤不能省略，发布直播和观看直播都要先loginchannel，
    // 登录同一个的channelID才能观看。
    ZegoUser *user = [[ZegoUser alloc]init];
    user.userName=@"12222";
    user.userID=@"111";

    [self.zegoAVApi loginChannel:@"12344444" user:user];
}

/// \brief 登录频道的回调
- (void)onLoginChannel:(NSString *)channel error:(uint32)err {
    if (err==0) {
        //设置view的背景图
        //[selpf setBackgroundImage:backgroundImage playerView:bigView];
        // 登录成功，开始播放视频
        [self.zegoAVApi setRemoteView:0 view:self.view];
        [self.zegoAVApi setRemoteViewMode:0 mode:ZegoVideoViewModeScaleAspectFill];
        bool ret = [self.zegoAVApi startPlayStream:@"12344444" viewIndex:0];
        NSLog(@"%d",ret);
        
    }else{
        // 登录频道失败，提示用户
        NSLog(@"登录失败");
    }
}

/// \brief 观看直播成功
/// \param streamID 直播流的唯一标识
- (void)onPlaySucc:(NSString *)streamID channel:(NSString *)channel
{
    NSLog(@"%s, streamID:%@", __func__, streamID);

}

/// \brief 视频的宽度和高度变化通知,startPlay后，如果视频宽度或者高度发生变化(首次的值也会)，则收到该通知，在收到onVideoSizeChanged的通知时去掉背景图，也是开始渲染的时候。
/// \param width 宽
/// \param height 高
- (void)onVideoSizeChanged:(NSString *)streamID width:(uint32)width height:(uint32)height
{
    NSLog(@"%s, streamID %@", __func__, streamID);
    
    // 获取播放streamID的view，清理view的background Image
//    UIView *view = self.viewContainersDict[streamID];
//    if (view){
//      [self setBackgroundImage:nil playerView:view];
//    }
    
}

/// \brief 观看直播失败
- (void)onPlayStop:(uint32)err streamID:(NSString *)streamID channel:(NSString *)channel {
    
    NSLog(@"%s, err: %u, stream: %@", __func__, err, streamID);
    
}

#pragma mark----结束直播
-(void)endOfTheBroadcast{
    // 结束直播
    [self.zegoAVApi stopPlayStream:@"12344444"];
    [self.zegoAVApi setRemoteView:0 view:nil];
    [self.zegoAVApi logoutChannel];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
