//
//  InterfaceURL.h
//  公安局项目
//
//  Created by 徐恒 on 16/3/11.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#ifndef InterfaceURL_h
#define InterfaceURL_h

#define BASE_URL @"http://119.29.136.56:8080/msxs-api"
//#define BASE_URL @"http://192.168.31.99:8080/msxs-api"
//上传图片URL
#define Image_URL @"http://101.200.186.153:8888/custom-file"
//#define Image_URL @"http://192.168.199.124"
//百度APIURL
#define BaiduMap_URL @"http://api.map.baidu.com/geosearch/v3/nearby"

#endif /* InterfaceURL_h */
