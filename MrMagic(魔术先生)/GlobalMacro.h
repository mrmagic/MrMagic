//
//  GlobalMacro.h
//  公安局项目
//
//  Created by 镇景雄 on 16/3/17.
//  Copyright © 2016年 aiweifeihong. All rights reserved.
//

#ifndef GlobalMacro_h
#define GlobalMacro_h

#define RONGCLOUD_IM_APPKEY @"ik1qhw091ivdp"
/**
 *  HUD自动隐藏
 *
 */
#define HUDNormal2(msg) {MBProgressHUD *hud=[MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].delegate window] animated:NO];\
hud.mode = MBProgressHUDModeText;\
hud.minShowTime=3;\
hud.detailsLabelText= msg;\
hud.detailsLabelFont = [UIFont systemFontOfSize:17];\
[hud hide:YES afterDelay:1];\
}


/**
 *  HUD自动隐藏
 *
 */
#define HUDNormal(msg) {MBProgressHUD *hud=[MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].delegate window] animated:NO];\
hud.mode = MBProgressHUDModeText;\
hud.minShowTime=1;\
hud.detailsLabelText= msg;\
hud.detailsLabelFont = [UIFont systemFontOfSize:17];\
[hud hide:YES afterDelay:1];\
}

/**
 *  HUD不自动隐藏
 *
 */
#define HUDNoStop(msg)    {MBProgressHUD *hud=[MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].delegate window] animated:NO];\
hud.minShowTime=1;\
hud.detailsLabelText = msg;\
hud.detailsLabelFont = [UIFont systemFontOfSize:17];\
hud.mode = MBProgressHUDModeIndeterminate;}


/**
 *  HUD不自动隐藏最小时间为0
 *
 */
#define HUDNoStop1(msg)    {MBProgressHUD *hud=[MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].delegate window] animated:NO];\
hud.detailsLabelText = msg;\
hud.detailsLabelFont = [UIFont systemFontOfSize:17];\
hud.mode = MBProgressHUDModeIndeterminate;}


/**
 *  HUD隐藏
 *
 */
#define HUDStop [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication].delegate window] animated:NO];

//获取设备的物理高度
#define ScreenHeight [UIScreen mainScreen].bounds.size.height
//获取设备的物理宽度
#define ScreenWidth [UIScreen mainScreen].bounds.size.width

#define DJWidth [UIScreen mainScreen].bounds.size.width
#define DJHeight [UIScreen mainScreen].bounds.size.height

#define UserDefault [NSUserDefaults standardUserDefaults]

#define setImage(ImageView,urlstr)   [ImageView sd_setImageWithURL:[NSURL URLWithString:urlstr] placeholderImage:[UIImage imageNamed:@"placeholder"]];
#define setButton(Button,urlstr) [Button sd_setImageWithURL:[NSURL URLWithString:urlstr] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder"]];

#define CREAT_COLOR(R,G,B,ALPHA) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:ALPHA]

#define requestIsSuccessful [responseDic[@"code"] integerValue]==1

#endif /* GlobalMacro_h */
