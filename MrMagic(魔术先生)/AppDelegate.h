//
//  AppDelegate.h
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/1.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZegoAVKit2/ZegoLiveApi.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) ZegoLiveApi *zegoAVApi;

@property (nonatomic,strong)NSMutableArray *userList;


@end

