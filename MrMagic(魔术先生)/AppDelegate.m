//
//  AppDelegate.m
//  MrMagic(魔术先生)
//
//  Created by 镇景雄 on 16/9/1.
//  Copyright © 2016年 镇景雄. All rights reserved.
//

#import "AppDelegate.h"
#import <ZegoAVKit2/ZegoLiveApi.h>
#import "RootViewController.h"
#import <RongIMLib/RongIMLib.h>
#import "RCDLive.h"
#import "RCDLiveGiftMessage.h"

NSData *g_signKey = nil;
uint32 g_appID = 0;

BOOL g_useTestEnv = NO;
BOOL g_requireHardwareAccelerated = YES;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController=[[RootViewController alloc]init];
    [self.window makeKeyAndVisible];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    //初始化即构SDK
    [self initJiGouSDK];
    //初始化融云SDK
    [self initIntegratingCloudSDK];
    
    return YES;
}

-(RCUserInfo *)parseUserInfoFormDic:(NSDictionary *)dic{
    RCUserInfo *user = [[RCUserInfo alloc]init];
    user.userId = [dic objectForKey: @"id" ];
    user.name = [dic objectForKey: @"name" ];
    user.portraitUri = [dic objectForKey: @"icon" ];
    return user;
}


#pragma makr-----初始化即构SDK
-(void)initJiGouSDK{
    // 设置日志级别, 只有调了这个APi, 才能生成日志 generic:3  debug:4
    [ZegoLiveApi setLogLevel:4];
    // 设置是否开启“测试环境”, true:开启 false:关闭
    [ZegoLiveApi setUseTestEnv:YES];
    
    // Demo 把signKey先写到代码中
    // ！！！注意：这个Appid和signKey需要从server下发到App，避免在App中存储，防止盗用
    Byte signkey[] = {0x76,0x7f,0x66,0x55,0xa4,0x47,0x0f,0xcb,0x98,0x4a,0xf3,0xdd,0xae,0x70,0x34,0xd1,0xe6,0x72,0x00,0xdd,0x35,0x27,0x95,0x0f,0xd1,0xb9,0x36,0x11,0xc7,0xeb,0x07,0x48};
    NSData * appSign = [[NSData alloc] initWithBytes:signkey length:32];
    uint appID = 3800527416;
    
    // 初始化SDK
    self.zegoAVApi = [[ZegoLiveApi alloc] initWithAppID:appID appSignature:appSign];
    
    // 设置是否开启“硬件加速”, true: 开启  false:关闭
    // ！！！打开硬编硬解，业务侧需要有后台控制开关，避免碰到版本升级或者硬件升级时出现硬编硬解失败的问题，若硬编失败我们会转成软编。
    [self.zegoAVApi requireHardwareAccelerated:false];

}

#pragma makr-----初始化融云SDK
-(void)initIntegratingCloudSDK{
    [[RCIMClient sharedRCIMClient] initWithAppKey:RONGCLOUD_IM_APPKEY];
    [[RCDLive sharedRCDLive] initRongCloud:RONGCLOUD_IM_APPKEY];
    //注册自定义消息
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"RoleList" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    _userList = [[NSMutableArray alloc]init];
    RCUserInfo *user = [self parseUserInfoFormDic:[data objectForKey:@"User1"]];
    [_userList  addObject:user];
    RCUserInfo *user2 = [self parseUserInfoFormDic:[data objectForKey:@"User2"]];
    [_userList  addObject:user2];
    RCUserInfo *user3 = [self parseUserInfoFormDic:[data objectForKey:@"User3"]];
    [_userList  addObject:user3];
    RCUserInfo *user4 = [self parseUserInfoFormDic:[data objectForKey:@"User4"]];
    [_userList  addObject:user4];
    RCUserInfo *user5 = [self parseUserInfoFormDic:[data objectForKey:@"User5"]];
    [_userList  addObject:user5];
    RCUserInfo *user6 = [self parseUserInfoFormDic:[data objectForKey:@"User6"]];
    [_userList  addObject:user6];
    RCUserInfo *user7 = [self parseUserInfoFormDic:[data objectForKey:@"User7"]];
    [_userList  addObject:user7];

}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
